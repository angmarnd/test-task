import Link from 'next/link'
import Head from 'next/head'
import styled from 'styled-components';

const Nav = styled.nav`
 position: fixed;
                height: 60px;
                 left: 0;
                 right: 0;
                 top:0;
                background: #222831;
                display: flex;
                justify-content: space-around;
                align-items: center;
                color: blue;
                // margin-bottom: 100px;
`;

const Main = styled.main`
margin-top: 60px;
`;

// const Button = styled.button`
// font-size: 20px;
// padding: 10px;
// border-radius: 3px;
// text-decaration: none

// color: ${(props) => (props.primary ? "white" : "palevioletred")};
// background: ${(props) => (props.primary ? "palevioletred" : "white")};

// `;

const LinkNav = styled.a`
text-decoration: none;
color: #eeeeee;
`;



export function MainLayout({ children }) {
    return (
        <>
            <Head>
                <title>LastFm</title>
                <link rel="icon" href="/favicon.ico" />
                <meta name='keywords' content='api last fm, music, audio' />
                <meta name='description' content='api last fm test project' />
                <meta charset='utf-8' />
            </Head>
            <Nav>
                <Link href={'/'}><LinkNav>Главная</LinkNav></Link>
                <Link href={'/a'}><LinkNav>Топ исполнителей</LinkNav></Link>
                <Link href={'/b'}><LinkNav>Топ треков</LinkNav></Link>
                <input />
            </Nav>
            <Main>
                {children}
            </Main>

        </>


    )
}