import axios from 'axios'
import Head from 'next/head'
import { MainLayout } from '../client/Components/MainLayout'
import TopArtists from '../pages/topArtist/index'
import styled from 'styled-components';

const Header = styled.h1`
text-align: center;
`;

export default function Home() {


  return (
    <MainLayout>

      <main>

        <Header>Top Artists:</Header>
        <TopArtists />

      </main>

    </MainLayout>
  )
}


