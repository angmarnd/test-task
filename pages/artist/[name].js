import { useState, useEffect } from "react";
import { useRouter } from 'next/router'
import { MainLayout } from "../../client/Components/MainLayout";

export default function ArtistPage({ artist: serverArtist, album: serverAlbum, track: serverTrack }) {
    const [artist, setArtist] = useState(serverArtist);
    const [album, setAlbum] = useState(serverAlbum);
    const [track, setTrack] = useState(serverTrack);

    const router = useRouter();
    useEffect(() => {
        async function load() {
            // Делаем запрос к эндпоинту
            const responceArtist = await fetch(`http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=${router.query.name}&api_key=b61705fd9ed8bdebe341bfa42de1ac6c&format=json`);
            const responceAlbum = await fetch(`http://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&artist=${router.query.name}&api_key=b61705fd9ed8bdebe341bfa42de1ac6c&format=json`);
            const responceTrack = await fetch(`http://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks&artist=${router.query.name}&api_key=b61705fd9ed8bdebe341bfa42de1ac6c&format=json`);

            // Парсим запрос
            const dataArtist = await responceArtist.json();
            const dataAlbum = await responceAlbum.json();
            const dataTrack = await responceTrack.json();
            setArtist(dataArtist)
            setAlbum(dataAlbum)
            setTrack(dataTrack)
        }
        if (!serverArtist) {
            load();
        }
    }, []);

    if (!artist) {
        return (
            <MainLayout>
                <p>Loading...</p>
            </MainLayout>
        )
    }

    return (
        <MainLayout>
            <title>{artist.artist.name}</title>

            <h1>Artist {router.query.name}</h1>
            <img src={artist.artist.image['#text'] ? artist.artist.image['#text'].find(({ size }) => size === 'large') : 'http://corpusletov.ancs.ru/wp-content/gallery/50011/300x300.gif'}></img>
            
            <p>Tags:</p>
            {artist.artist.tags.tag.map((el) => (
                <div>
                    {el.name}
                </div>
            ))}

            <p>Albums: </p>
            {album ? album.topalbums.album.map((el) => (
                <div>{el.name}</div>
            )) : '[Message]: Empty'}

            <p>Tracks: </p>
            {track ? track.toptracks.track.slice(0,18).map((el) => (
                <div>{el.name}</div>
            )) : '[Message]: Empty'}
        </MainLayout>
    )
}

ArtistPage.getInitialProps = async ({ query, req }) => {
    try {
        // Делаем запрос к эндпоинту
     
        const responceArtist = await fetch(`http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=${query.name}&api_key=b61705fd9ed8bdebe341bfa42de1ac6c&format=json`);
        const responceAlbum = await fetch(`http://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&artist=${query.name}&api_key=b61705fd9ed8bdebe341bfa42de1ac6c&format=json`);
        const responceTrack = await fetch(`http://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks&artist=${query.name}&api_key=b61705fd9ed8bdebe341bfa42de1ac6c&format=json`);
        // Парсим запрос
        const artist = await responceArtist.json();
        const album = await responceAlbum.json();
        const track = await responceTrack.json();
        return {
            props: artist,
            props2: album,
            props3: track
        }

    } catch (error) {
        console.error(error);
    }
}