import { useState, useEffect } from 'react'
import { MainLayout } from "../../client/Components/MainLayout";
import Link from 'next/link';

import styled from 'styled-components';


const Wrapper = styled.div`
display: flex;
flex-wrap: wrap;
justify-content: center;
`;

const ArtistCard = styled.div`
position: relative;
background-color: black;
border-radius: 3px;
box-shadow: 3px 3px 5px rgba(0, 0, 0, 0.1);
margin: 1rem;
width: 300px;
transition: 0.4s;
cursor: pointer;

&:hover{
opacity: 0.5;
}
`;

const Url = styled.div`
text-decaration: none;
`;

const Img = styled.img`
  width: 100%;
  height: 300px;
  width: 300px;
  object-fit: cover;
  overflow: hidden; 
`;

const Info = styled.div`
display: flex;
align-items: center;
justify-content: space-around;
padding: 1rem;
overflow: hidden;
color: white;
`;


export default function TopArtists({ topArtists: serverTopArtists }) {
    const [topArtists, setTopArtists] = useState(serverTopArtists);

    useEffect(() => {

        async function load() {
            const responce = await fetch(`http://ws.audioscrobbler.com/2.0/?method=chart.gettopartists&api_key=b61705fd9ed8bdebe341bfa42de1ac6c&format=json`);
            const data = await responce.json();
            setTopArtists(data.artists.artist.slice(0, 3))
        }
        if (!serverTopArtists) {
            load();
        }
    }, [])
    if (!topArtists) {
        return <MainLayout>
            <p>Loading...</p>
        </MainLayout>
    }
    return (

        <Wrapper>
            {
                topArtists.length >= 1 ? topArtists.map((topArtist, idx) => {
                    return (
                        <ArtistCard>
                            <Link href={`/artist/[name]`} as={`/artist/${topArtist.name}`}>
                                <Url href={topArtist.url}>
                                    <img src='http://corpusletov.ancs.ru/wp-content/gallery/50011/300x300.gif'></img>
                                    <Info key={idx}>
                                        {topArtist.name}
                                    </Info>
                                </Url>
                            </Link>

                        </ArtistCard>

                    )
                })
                    : '[Message]: Empty'}
        </Wrapper>
    )
}

TopArtists.getInitialProps = async (req) => {

    try {
        const responce = await fetch('http://ws.audioscrobbler.com/2.0/?method=chart.gettopartists&api_key=b61705fd9ed8bdebe341bfa42de1ac6c&format=json');

        const topArtists = await responce.json();
        return {
            topArtists: topArtists.artists.artist.slice(0, 3),
        }
    }
    catch (error) {
        console.log(error);
    }

}