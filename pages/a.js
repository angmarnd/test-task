import axios from "axios";
import { useState } from 'react';

export default function A() {
    const [album, setAlbum] = useState([]);
    const getTopArtist = (e) => {
        e.preventDefault();

        axios.get('http://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&artist=bts&api_key=b61705fd9ed8bdebe341bfa42de1ac6c&format=json')
            .then(res => setAlbum(res.data.topalbums.album))
            .catch(err => console.log(err));

    }
    return (
        <>
            <h1>A</h1>
            <button onClick={getTopArtist}>Get top artist BTS</button>
            {album.length >= 1 ? album.map((al, idx) => {
                return <p key={idx}>{al.name}</p>
            })
                : 'no one'}
        </>
    );
}